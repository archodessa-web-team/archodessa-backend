/**
 * ArchitecturalObject.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name:{
      type:"string",
      required:true,
      minLength: 2
    },
    address:{
      type:"string",
      required:"true"
    },
    address_old:{
      type:"string"
    },
    coordinates:{
      type:"string"
    },
    author: {
      model: 'Person'
    },
    type: {
      model: 'ArchitecturalType'
    },
    levels:{
      type:"string"
    },
    styles: {
      collection: 'ArchitecturalStyle',
      via: 'objects',
      dominant: true
    },
    elements: {
      collection: 'ArchitecturalElement',
      via: 'objects',
      dominant: true
    }
  }
};

