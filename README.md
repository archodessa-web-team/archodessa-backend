# Бэкенд-часть системы управления объектами Архитектуры Одессы

Для работы использует [Sails.js](http://sailsjs.org/) в связке с [MongoDb](https://www.mongodb.com/)

- [Планы и генеральное видение](https://bitbucket.org/archodessa-web-team/archodessa-frontend)
- [Баги и фичи](https://bitbucket.org/archodessa-web-team/archodessa-backend/issues)

## Запуск приложения

Установите ```mongo```, ```node``` и ```npm``` глобально

```bash
npm i // устанавливаем зависимости
npm start // запускаем базу данных и сервер
```

Если все плохо, запустите базу данных и сервер раздельно

```bash
    npm run db // запускаем базу данных
    sails lift // запускаем сервер
```

После запуска сервера запустите приложение [archodessa-frontend](https://bitbucket.org/archodessa-web-team/archodessa-frontend).

## Разработка

Sails.js позволяет просто описывать модели и связи между ними, генерируя на основе этого бэкенд для MongoDB. Для понимания принципов работы посмотрите в папку ```api/models```. После этого открывайте [документацию](http://sailsjs.org/documentation/concepts/) и начинайте писать.

### Как добавить модель

```bash
    sails generate api <foo>
```

Эта команда делает магию и создает два нужных файла ```api/models/Foo.js``` и ```api/controllers/FooController.js```. Про это можно почитать на сайте в [документации](http://sailsjs.org/documentation/reference/command-line-interface/sails-generate).

После этого в файле модели надо описать все необходимые поля и зависимости и перезапустить проект.

## Я могу! Я хочу!

Можешь? Хочешь? Знаешь, как сделать лучше? Заметил говно и умеешь исправить?

Открой [предложение](https://bitbucket.org/archodessa-web-team/archodessa-backend/issues) или пришли патч.
